# Speech Grapics Test - Cognito Login
This repository contains the Lambda function and relevant node modules used to return a Cognito Bearer token.

## Modules Used
The language used for the function was NodeJS and it makes use of the npm module `simple-oauth2`. It also makes use of the AWS SDK to get the Client details from the Parameter Store.
The CI/CD pipelines are ran within this repository.

## Function Details
The function will pull down the Client ID and Secret from SSM, and form a Credentials configuration to be used by the simple-oauth2 module, it will call the `getToken` function to retrieve and return a Bearer token.

Inputs: `tokenScope` (optional) - this will be a string specifying the scope of the token to be returned

Intended Output: Bearer token - Example:
```
{
    "access_token": "eyJraWQiOiI5bWRReThyMnE0RXVBMlBsZVJjVkZXSnZzbWFRamdZdHJRVXBMaG80b0pVPSIsImFsZyI6IlJTMjU2In0.eyJzdWIiOiI3cmgybm05NDZwdHM1b2MzbW1lcWRqa2dtZSIsInRva2VuX3VzZSI6ImFjY2VzcyIsInNjb3BlIjoiZ3JhcGhpY3NcL3VwZGF0ZSBncmFwaGljc1wvcmVhZCIsImF1dGhfdGltZSI6MTYxMjg4MTI1OCwiaXNzIjoiaHR0cHM6XC9cL2NvZ25pdG8taWRwLmV1LXdlc3QtMi5hbWF6b25hd3MuY29tXC9ldS13ZXN0LTJfald4dGc3TXhyIiwiZXhwIjoxNjEyODg0ODU4LCJpYXQiOjE2MTI4ODEyNTgsInZlcnNpb24iOjIsImp0aSI6ImFjZWI5YzYzLTMwZDYtNDEyMS1hZmIwLTIyY2NhZjU5YTY2ZSIsImNsaWVudF9pZCI6IjdyaDJubTk0NnB0czVvYzNtbWVxZGprZ21lIn0.Uj-LB27ee7d43gdxzvGoOVNIvAJmWmW6Rm8kYZhvKu7wS93OPEfuWKWvsT_rWscIduBbrgwSp0ZI7ah3arXuvaDuYSlSErKk1vtylSXCMGmj7FtI6NkexvsKquls3RTGeo7TnFlKhxV6KhgxdlSaf2F9BS44RXnDt5mSWzWb3cf-UBUGKhDGE5r24qXY98AwVpoukywc6VYiLt77ELv37mGdf_90FPdHwiWWDW73Y09wKlriz9X1I4BvIzRGZeboj-wWibpI7vsf1kkyizLOG9i_NeT3pLXHdj_DArTXpqvmJMjaCJmJWSLft_MqSyFOMGfU3S6itsBVliH2mdvKnA",
    "expires_in": 3600,
    "token_type": "Bearer",
    "expires_at": "2021-02-09T15:34:18.022Z"
}
```

This token can be viewed in more detail at [JWT.io](https://JWT.io)

## CI/CD Process
Upon merging into master, it will package up the function with the mode modules, and use the version number in the `package.json` to store this ZIP file in its corresponding s3 bucket. It will
also update the .txt file containing the current version, also contained in S3, which is used by the terraform when applying. The process will also update the code currently in Lambda. There is 
a manual step to kick off the terraform master pipeline, however this is unnecessary if just testing code changes, as this is handled in the previous step.

## Improvements / Difficulties
A significant pain point for me was trying to get the function to interact with Cognito directly via the AWS SDK and exploring the different types of Auth flow. I had tried to initially
go down the path of a user sending a Username/Password but that seemed to point towards a front-end system utilising a module like Amplify. Going forward, an improvement I would make is to
maintain a session, by storing a token and any call going forward would check to see its validity, before returning it/fetching a new token.

## Testing
Testing is detailed in the README in the [Terraform](https://gitlab.com/speech-graphics-assessment/terraform) repository.
