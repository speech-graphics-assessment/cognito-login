const {ClientCredentials} = require('simple-oauth2');
const AWS = require('aws-sdk');

AWS.config.update({
  region: 'eu-west-2'
});

const parameterStore = new AWS.SSM();

const getParam = param => {
  return new Promise((res, rej) => {
    parameterStore.getParameter({
      Name: param,
      WithDecryption: true
    }, (err, data) => {
      if (err) {
        return rej(err)
      }
      return res(data)
    })
  })
};

exports.handler = async (event) => {

  const ClientID = await getParam('ClientID');
  const ClientSecret = await getParam('ClientSecret');

  const config = {
    client: {
      id: ClientID.Parameter.Value,
      secret: ClientSecret.Parameter.Value,
    },
    auth: {
      tokenHost: 'https://sg-assessment-secret.auth.eu-west-2.amazoncognito.com/',
      tokenPath: 'oauth2/token',
    },
    http: {
      headers: {
        'cache-control': 'no-cache',
        'content-type': 'application/x-www-form-urlencoded',
      },
    },

  };

  const client = new ClientCredentials(config);
  const tokenParams = {
    scope: event.tokenScope
  };

  try {
    const accessToken = await client.getToken(tokenParams)
    return accessToken;
  } catch (error) {
    return {
      info: 'Access Token error',
      message: error.message,
    }
  }

};
